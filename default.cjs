function defaults(testObject, inventory) {

    for (let item in inventory) {
        if (item in testObject == false)
            testObject[item] = inventory[item]
    }
    return testObject

}

module.exports = defaults


