function invert(inventory) {
    let reverse = {}
    for (let keys in inventory) {
        reverse[inventory[keys]] = keys
    }
    return reverse

}
module.exports = invert