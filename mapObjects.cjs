function mapObjects(inventory, Callback) {
    for (let values in inventory) {
        inventory[values] = Callback(inventory[values])
    }
    return inventory

}
module.exports = mapObjects